﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Electricity.Startup))]
namespace Electricity
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
