﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Electricity
{
    public partial class ElectricityScreen : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime CurrentDate = DateTime.Now;
                txtDate.Text = CurrentDate.ToString("dd/MM/yyyy");
            }
        }

        protected void ddlTariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTariff.SelectedValue == "Residential")
            {
                ddlTariffScale.Items.Clear();

                ListItem lstItem0 = new ListItem("Please select a tariff scale", "Please select a tariff scale");
                ddlTariffScale.Items.Insert(0, lstItem0);

                ListItem lstItem1 = new ListItem("3", "3");
                ddlTariffScale.Items.Insert(1, lstItem1);

                ListItem lstItem2 = new ListItem("4", "4");
                ddlTariffScale.Items.Insert(2, lstItem2);

                ListItem lstItem3 = new ListItem("8", "8");
                ddlTariffScale.Items.Insert(3, lstItem3);

                ListItem lstItem4 = new ListItem("9", "9");
                ddlTariffScale.Items.Insert(4, lstItem4);

                ListItem lstItem5 = new ListItem("RTOU", "RTOU");
                ddlTariffScale.Items.Insert(5, lstItem5);

                ddlTariff.DataBind();

                ddlTariffScale.Visible = true;
                txtTariffScale.Visible = false;

            }
            else if (ddlTariff.SelectedValue == "Business and General")
	        {
                ddlTariffScale.Items.Clear();

                ListItem lstItem0 = new ListItem("Please select a tariff scale", "Please select a tariff scale");
                ddlTariffScale.Items.Insert(0, lstItem0);

                ListItem lstItem1 = new ListItem("1", "1");
                ddlTariffScale.Items.Insert(1, lstItem1);

                ListItem lstItem2 = new ListItem("10", "10");
                ddlTariffScale.Items.Insert(2, lstItem2);

                ListItem lstItem3 = new ListItem("11", "11");
                ddlTariffScale.Items.Insert(3, lstItem3);

                ddlTariffScale.Visible = true;
                txtTariffScale.Visible = false;
	        }
            else if (ddlTariff.SelectedValue == "Bulk")
	        {
                ddlTariffScale.Items.Clear();

                ListItem lstItem0 = new ListItem("Please select a tariff scale", "Please select a tariff scale");
                ddlTariffScale.Items.Insert(0, lstItem0);

                ListItem lstItem1 = new ListItem("CTOU", "CTOU");
                ddlTariffScale.Items.Insert(1, lstItem1);

                ListItem lstItem2 = new ListItem("ITOU", "ITOU");
                ddlTariffScale.Items.Insert(2, lstItem2);

                ddlTariffScale.Visible = true;
                txtTariffScale.Visible = false;
	        }
            else if (ddlTariff.SelectedValue == "Other")
            {
                ddlTariffScale.Items.Clear();

                ListItem lstItem0 = new ListItem("No scale", "No scale");
                ddlTariffScale.Items.Insert(0, lstItem0);
               
                ddlTariffScale.Visible = false;
                txtTariffScale.Visible = true;
                
            }
        }

        protected void checkboxAddressNotFound_CheckedChanged(object sender, EventArgs e)
        {
            if (checkboxAddressNotFound.Checked == true)
            {
                FileUpload1.Visible = true;
                SSDetails1.Visible = false;
                SSDetails2.Visible = false;
                SSDetails3.Visible = false;
                
            }
            else if (checkboxAddressNotFound.Checked == false)
            {
                FileUpload1.Visible = false;
                SSDetails1.Visible = true;
                SSDetails2.Visible = true;
                SSDetails3.Visible = true;
            }
        }

        protected void ddlPrefferredContactMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPrefferredContactMethod.SelectedValue == "TelWork")
            {
                if (txtTelNoWork.Text == string.Empty)
                {
                    string msg = "Enter work telphone number";
                    string script = "alert('" + msg + "');";
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Status", script, true);

                    ddlPrefferredContactMethod.SelectedIndex = 0;
                }
            }
            else if (ddlPrefferredContactMethod.SelectedValue == "TelHome")
            {
                if (txtTelNoHome.Text == string.Empty)
                {
                    string msg = "Enter home telphone number";
                    string script = "alert('" + msg + "');";
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Status", script, true);

                    ddlPrefferredContactMethod.SelectedIndex = 0;

                }
            }
            else if (ddlPrefferredContactMethod.SelectedValue == "CellNo")
            {
                if (txtCellNo.Text == string.Empty)
                {
                    string msg = "Enter cell number";
                    string script = "alert('" + msg + "');";
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Status", script, true);

                    ddlPrefferredContactMethod.SelectedIndex = 0;

                }
            }
            else if (ddlPrefferredContactMethod.SelectedValue == "Fax")
            {
                if (txtFaxNo.Text == string.Empty)
                {
                    string msg = "Enter Fax number";
                    string script = "alert('" + msg + "');";
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Status", script, true);

                    ddlPrefferredContactMethod.SelectedIndex = 0;

                }
            }
            else if (ddlPrefferredContactMethod.SelectedValue == "Email")
            {
                if (txtEmail.Text == string.Empty)
                {
                    string msg = "Enter email address";
                    string script = "alert('" + msg + "');";
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Status", script, true);

                    ddlPrefferredContactMethod.SelectedIndex = 0;

                }
            }
        }

        protected void btnDone1_Click(object sender, EventArgs e)
        {
            if (ddlAppType.SelectedIndex == 0)
            {
                string msg = "Please select application type";
                string script = "alert('" + msg + "');";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Status", script, true);
            }
            else if (ddlTitle.SelectedIndex == 0)
            {
                string msg = "Please select a title";
                string script = "alert('" + msg + "');";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Status", script, true);
            }
            else if (txtName.Text == string.Empty)
            {
                string msg = "Please enter name";
                string script = "alert('" + msg + "');";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Status", script, true);
            }
            else if (txtIdNo.Text == string.Empty)
            {
                string msg = "Please enter ID number";
                string script = "alert('" + msg + "');";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Status", script, true);
            }
            else if (ddlPrefferredContactMethod.SelectedIndex == 0)
            {
                string msg = "Please select a preffered contact method";
                string script = "alert('" + msg + "');";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Status", script, true);
            }
        }

        protected void ddlAppType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAppType.SelectedValue == "ChangesToExistingConnections")
            {
                PrefferedConnectionType.Visible = false;
                TypeOfSupply.Visible = false;
            }
            else if (ddlAppType.SelectedValue == "NewConnections")
            {
                PrefferedConnectionType.Visible = true;
                TypeOfSupply.Visible = true;
            }
            else
            {
                PrefferedConnectionType.Visible = true;
                TypeOfSupply.Visible = true;
            }
        }

    }
}