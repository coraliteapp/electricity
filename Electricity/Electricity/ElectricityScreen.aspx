﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="ElectricityScreen.aspx.cs" Inherits="Electricity.ElectricityScreen" MaintainScrollPositionOnPostback="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxControlToolkit" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style1
        {
            color: #000000;
            font-size: x-large;
        }
        .style2
        {
            font-size: medium;
        }
        .style3
        {
            font-size: medium;
        }
        .style4
        {
            font-size: medium;
        }
    </style>

    <script type="text/javascript">
        function CheckNumeric(e) {
            if (window.event) // IE 
            {
                //alert(e.keyCode);

                //if (e.keyCode == 44 || e.keyCode == 46)
                if (e.keyCode == 46) {

                }
                else if ((e.keyCode < 48 || e.keyCode > 57) & e.keyCode != 8) {
                    event.returnValue = false;
                    return false;

                }
            }
            else { // Fire Fox
                if ((e.which < 48 || e.which > 57) & e.which != 8) {
                    e.preventDefault();
                    return false;

                }
            }
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
   <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <h1 class="style1"><strong>Electricity</strong></h1>

    <%--<table class="style1">
        <tr>
            <td class="style2">
                <asp:Label ID="Label1" runat="server" Text="E-Number :" 
                    style="color: #000000; font-size: medium"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblENumber" runat="server" Text="15891" CssClass="style2"></asp:Label>
            </td>
        </tr>
    </table>--%>

    <asp:Panel ID="LogDetails" runat="server" GroupingText="Log Details" 
        Width="100%" style="font-size: medium">

        <table class="style1">
            <tr>
                <td class="style2">
                    <asp:Label ID="Label2" runat="server" Text="Logged by :"></asp:Label>
                </td>
                <td >
                    <asp:TextBox ID="txtLoggedBy" runat="server" CssClass="style2" Width="50%" Enabled="false" Text="Abrie Cronje"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label3" runat="server" Text="Date :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDate" runat="server" CssClass="style2" Width="50%" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>

    </asp:Panel>

    <asp:Panel ID="Particulars" runat="server" CssClass="tooltip" ToolTip="Only the property owner is authorised to make changes to their electrical connection. If land is being developed and has no owner, the developer must write their details. At this step owner/applicant will have to select an application type e.g. New Connection"
        GroupingText="Particulars Of Property Owner/Application <img src='Images/help.png' Width='25px' Height='25px'/>" Width="100%" style="font-size: medium">
        
        <table class="style1">
            <tr>
                <td class="style2">
                    <asp:Label ID="Label4" runat="server" Text="Application Type : *" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlAppType" runat="server" Width="50%" CssClass="style2" OnSelectedIndexChanged="ddlAppType_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="0">Please select relevant application type...</asp:ListItem>
                        <asp:ListItem Value="NewConnections">New Connections</asp:ListItem>
                        <asp:ListItem Value="ChangesToExistingConnections">Changes To Existing Connections</asp:ListItem>
                        <asp:ListItem Value="ReinstatementOfsupply">Reinstatement of supply</asp:ListItem>
                        <asp:ListItem Value="PowerEnquiry">Power Enquiry</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <%--<td rowspan="10" style="width:25%;">
                    <asp:Label ID="Label1" runat="server" Text="Only the property owner is authorised to make changes to their electrical connection. If land is being developed and has no owner, the developer must write their details. At this step owner/applicant will have to select an application type e.g. New Connection" style="font-size: small"></asp:Label>
                </td>--%>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label5" runat="server" Text="Title : *" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTitle" runat="server" Width="50%" CssClass="style2">
                        <asp:ListItem Value="0">Please select a title...</asp:ListItem>
                        <asp:ListItem Value="1">Mr.</asp:ListItem>
                        <asp:ListItem Value="2">Mrs.</asp:ListItem>
                        <asp:ListItem Value="3">Ms.</asp:ListItem>
                        <asp:ListItem Value="4">Dr.</asp:ListItem>
                        <asp:ListItem Value="5">Prof.</asp:ListItem>
                        <asp:ListItem Value="6">Ps.</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label6" runat="server" Text="Name : *" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
                <%--<td>
                    <asp:Button ID="btnSearch" runat="server" Text="Search"/>
                </td>--%>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label7" runat="server" Text="ID No : *" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtIdNo" runat="server" Width="50%" CssClass="style2" onkeypress="CheckNumeric(event);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label9" runat="server" Text="Tel No (W) :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtTelNoWork" runat="server" Width="50%" CssClass="style2" onkeypress="CheckNumeric(event);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label10" runat="server" Text="Tel No (H) :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtTelNoHome" runat="server" Width="50%" CssClass="style2" onkeypress="CheckNumeric(event);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label11" runat="server" Text="Cell No :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCellNo" runat="server" Width="50%" CssClass="style2" onkeypress="CheckNumeric(event);"></asp:TextBox>
                </td>
            </tr>
            <tr >
                <td class="style2">
                    <asp:Label ID="Label12" runat="server" Text="Fax No :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFaxNo" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label13" runat="server" Text="Email Address :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" Width="50%" CssClass="style2" TextMode="Email"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label8" runat="server" Text="Preffered Contact Method : *" Width = "100%"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlPrefferredContactMethod" runat="server" Width="50%" 
                        CssClass="style2" 
                        onselectedindexchanged="ddlPrefferredContactMethod_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="0">How would you like to receive notifications ...</asp:ListItem>
                        <asp:ListItem Value="TelWork">Tel No (W)</asp:ListItem>
                        <asp:ListItem Value="TelHome">Tel No (H)</asp:ListItem>
                        <asp:ListItem Value="CellNo">Cell No</asp:ListItem>
                        <asp:ListItem Value="Fax">Fax</asp:ListItem>
                        <asp:ListItem Value="Email">Email</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    
                </td>
                <td>
                    <asp:Button ID="btnDone1" runat="server" Text="Done" OnClick="btnDone1_Click" />
                </td>
            </tr>
        </table>
        

    </asp:Panel>

    <asp:Panel ID="SiteSupplyAffected" runat="server" CssClass="tooltip" ToolTip="This is the address of where the electrical connection is affected (e.g. installed or modified). There shall be an integration with Google maps if address can be found there shall be an attachment where the owner/applicant can draw a locality sketch and upload it."
        GroupingText="Site Supply Affected <img src='Images/help.png' Width='25px' Height='25px'/>" style="font-size: medium">

        <table id ="SSDetails1" runat="server">
            <tr>
                <td style="width:8%;">
                    <asp:Label ID="Label17" runat="server" Text="Unit No :"></asp:Label>
                </td>
                <td style="width:7%;">
                    <asp:TextBox ID="txtSSAUnitNo" runat="server" Width="90%" CssClass="style2"></asp:TextBox>
                </td>

                <td style="width:8%">
                    <asp:Label ID="Label14" runat="server" Text="Floor No :"></asp:Label>
                </td>
                <td style="width:7%;">
                    <asp:TextBox ID="txtSSAFloorNo" runat="server" Width="90%" CssClass="style2"></asp:TextBox>
                </td>

                <td style="width:10%">
                    <asp:Label ID="Label15" runat="server" Text="Section No :"></asp:Label>
                </td>
                <td style="width:7%;">
                    <asp:TextBox ID="txtSSASectionNo" runat="server" Width="90%" CssClass="style2"></asp:TextBox>
                </td>

                <td style="width:17%">
                    <asp:Label ID="Label16" runat="server" Text="Building/Complex :"></asp:Label>
                </td>
                <td style="width:25%">
                    <asp:TextBox ID="txtSSABuildingComplex" runat="server" Width="100%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            </table>
        <table id ="SSDetails2" runat="server">
            <tr>
                <td style="width:8%;">
                    <asp:Label ID="Label30" runat="server" Text="Street No :"></asp:Label>
                </td>
                <td style="width:6%;">
                    <asp:TextBox ID="txtStreetNo" runat="server" Width="90%" CssClass="style2"></asp:TextBox>
                </td>

                <td style="width:10%">
                    <asp:Label ID="Label45" runat="server" Text="Street Name :"></asp:Label>
                </td>
                <td style="width:30%;">
                    <asp:TextBox ID="txtStreetName" runat="server" Width="90%" CssClass="style2"></asp:TextBox>
                </td>

                <td style="width:10%">
                    <asp:Label ID="Label46" runat="server" Text="Street Type :"></asp:Label>
                </td>
                <td style="width:20%;">
                    <asp:DropDownList ID="DropDownList1" runat="server" Width="90%" CssClass="style2">
                    <asp:ListItem Value="0">Please select road type</asp:ListItem>
                        <asp:ListItem Value="1">Road</asp:ListItem>
                        <asp:ListItem Value="2">Street</asp:ListItem>
                        <asp:ListItem Value="3">Avenue</asp:ListItem>
                        <asp:ListItem Value="4">Lane</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            </table>
        <table class="style1" id ="SSDetails3" runat="server">
            <%--<tr>
                <td class="style2">
                    <asp:Label ID="Label18" runat="server" Text="Address :" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSSAAddress" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>--%>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label21" runat="server" Text="Suburb/District :" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSSASuburbDistrict" runat="server" Width="50%" 
                        CssClass="style3"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label22" runat="server" Text="City/Town :" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSSACityTown" runat="server" Width="50%" CssClass="style3"></asp:TextBox>
                </td>
            </tr>            
            <tr>
                <td class="style2">
                    <asp:Label ID="Label23" runat="server" Text="Code :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSSACode" runat="server" Width="50%" CssClass="style3"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label27" runat="server" Text="Erf No :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtErfNo" runat="server" Width="50%" CssClass="style3"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table style="width:100%;">
            <tr style="width:100%;">
                <td style="width:50%;">
                    <asp:CheckBox ID="checkboxAddressNotFound" runat="server" 
                        Text="Address not found" CssClass="style2" 
                        oncheckedchanged="checkboxAddressNotFound_CheckedChanged" AutoPostBack="true"/>
                </td>
                <td style="width:50%;"">
                    <asp:FileUpload ID="FileUpload1" runat="server" CssClass="style2" Visible="false"/>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    
                </td>
                <td>
                    <asp:Button ID="btnDone2" runat="server" Text="Done"/>
                </td>
            </tr>
        </table>

    </asp:Panel>

    <asp:Panel ID="ElectricalRepresentive" runat="server" CssClass="tooltip" ToolTip="Appointed Electrical Contractors details, together with relevant technical information to be completed on the Application form. For informal dwellings electrical representative field not required"
        GroupingText="Electrical Representive <img src='Images/help.png' Width='25px' Height='25px'/>" style="font-size: medium">
        <table class="style1">
            
            <tr>
                <td class="style2">
                    <asp:Label ID="Label50" runat="server" Text="Electrical Representative Name :" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtERName" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="width:8%;">
                    <asp:Label ID="Label20" runat="server" Text="Unit No :"></asp:Label>
                </td>
                <td style="width:7%;">
                    <asp:TextBox ID="txtERUnitNo" runat="server" Width="90%" CssClass="style2"></asp:TextBox>
                </td>

                <td style="width:8%">
                    <asp:Label ID="Label24" runat="server" Text="Floor No :"></asp:Label>
                </td>
                <td style="width:7%;">
                    <asp:TextBox ID="txtERFloorNo" runat="server" Width="90%" CssClass="style2"></asp:TextBox>
                </td>

                <td style="width:10%">
                    <asp:Label ID="Label25" runat="server" Text="Section No :"></asp:Label>
                </td>
                <td style="width:7%;">
                    <asp:TextBox ID="txtERSectionNo" runat="server" Width="90%" CssClass="style2"></asp:TextBox>
                </td>

                <td style="width:17%">
                    <asp:Label ID="Label26" runat="server" Text="Building/Complex :"></asp:Label>
                </td>
                <td style="width:25%">
                    <asp:TextBox ID="txtERBuildingComplex" runat="server" Width="100%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="width:8%;">
                    <asp:Label ID="Label18" runat="server" Text="Street No :"></asp:Label>
                </td>
                <td style="width:6%;">
                    <asp:TextBox ID="TextBox1" runat="server" Width="90%" CssClass="style2"></asp:TextBox>
                </td>

                <td style="width:10%">
                    <asp:Label ID="Label19" runat="server" Text="Street Name :"></asp:Label>
                </td>
                <td style="width:30%;">
                    <asp:TextBox ID="TextBox2" runat="server" Width="90%" CssClass="style2"></asp:TextBox>
                </td>

                <td style="width:10%">
                    <asp:Label ID="Label47" runat="server" Text="Street Type :"></asp:Label>
                </td>
                <td style="width:20%;">
                    <asp:DropDownList ID="DropDownList2" runat="server" Width="90%" CssClass="style2">
                    <asp:ListItem Value="0">Please select road type</asp:ListItem>
                        <asp:ListItem Value="1">Road</asp:ListItem>
                        <asp:ListItem Value="2">Street</asp:ListItem>
                        <asp:ListItem Value="3">Avenue</asp:ListItem>
                        <asp:ListItem Value="4">Lane</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            </table>
        <table class="style1">
            <%--<tr>
                <td class="style2">
                    <asp:Label ID="Label19" runat="server" Text="Address :" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtERAddress" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>--%>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label31" runat="server" Text="Suburb/District :" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtERSuburbDistrict" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label32" runat="server" Text="City/Town :" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtERCityTown" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label33" runat="server" Text="Code :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtERCode" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            
            <tr>
                <td class="style2">
                    <asp:Label ID="Label35" runat="server" Text="Tel No (W) :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtERTelNoWork" runat="server" Width="50%" CssClass="style2" onkeypress="CheckNumeric(event);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label36" runat="server" Text="Tel No (H) :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtERTellNoHome" runat="server" Width="50%" CssClass="style2" onkeypress="CheckNumeric(event);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label37" runat="server" Text="Cell No :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtERCellNo" runat="server" Width="50%" CssClass="style2" onkeypress="CheckNumeric(event);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label38" runat="server" Text="Fax No :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtERFaxNo" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label39" runat="server" Text="Email Address :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEREmail" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label34" runat="server" Text="Registration No :" Width = "100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtERRegistration" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    
                </td>
                <td>
                    <asp:Button ID="btnDone3" runat="server" Text="Done"/>
                </td>
            </tr>
        </table>

    </asp:Panel>

    <asp:Panel ID="PrefferedConnectionType" runat="server" CssClass="tooltip" ToolTip="Complete this for new connections, select the radio the button of the connection type you would prefer e.g. Overhead/Underground This choice might not always be available as it’s dependent on the existing electrical supply in the area." 
        GroupingText="Preffered Connection Type <img src='Images/help.png' Width='25px' Height='25px'/>" Width="100%" 
        style="font-size: medium">

        <table class="style1">
            <tr>
                <td class="style2">

                    <asp:RadioButtonList ID="rbtnPrefferredConnectionType" runat="server" CssClass="style2" RepeatDirection="Horizontal" Width="100%">
                        <asp:ListItem Value="Overhead">Overhead</asp:ListItem>
                        <asp:ListItem Value="Underground">Underground</asp:ListItem>
                    </asp:RadioButtonList>
                    
                </td>
                <td>
                    <%--<asp:RadioButton ID="rdbtnUnderground" runat="server" Text="Underground" CssClass="style2"/>--%>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label28" runat="server" Text="Reason for change :" Width = "100%"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlReasonForChange" ToolTip="Do not complete unless changes to existing connection application type is selected e.g. (Reason for change, meter and connection number)." runat="server" Width="50%" CssClass="style2 tooltip">
                        <asp:ListItem Value="0">Please select reason for change...</asp:ListItem>
                        <asp:ListItem Value="Overhead & Underground">Overhead & Underground</asp:ListItem>
                        <asp:ListItem Value="Boundary Metering">Boundary Metering</asp:ListItem>
                        <asp:ListItem Value="Service Cable">Service Cable</asp:ListItem>
                        <asp:ListItem Value="Demand Change">Demand Change</asp:ListItem>
                        <asp:ListItem Value="Demand Change">Tariff Change</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label29" runat="server" Text="Existing meter number :" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtExistingMeterNumber" runat="server" Width="50%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label48" runat="server" Text="Meter Type :"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlMeterType" ToolTip="Select any of the three options on the dropdown e.g. Credit; Two Way & Prepaid (Wireless)" runat="server" Width="50%" CssClass="style2 tooltip">
                    <asp:ListItem Value="0">Please select relevant Meter Type...</asp:ListItem>
                        <asp:ListItem Value="1">Credit</asp:ListItem>
                        <asp:ListItem Value="2">Two way</asp:ListItem>
                        <asp:ListItem Value="3">Prepaid (Wireless)</asp:ListItem>
                        <asp:ListItem Value="4">Bidirectional</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label49" runat="server" Text="Date Supply Required :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDateSupplyRequired" ToolTip="This is the date you require the electrical connection to be installed" runat="server" Width="50%" CssClass="style2 tooltip"></asp:TextBox>
                            <AjaxControlToolkit:CalendarExtender Format="yyyy/MM/dd" ID="txtDateSupplyRequired_CalendarExtender"
                                runat="server" Enabled="True" TargetControlID="txtDateSupplyRequired">
                            </AjaxControlToolkit:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    
                </td>
                <td>
                    <asp:Button ID="btnDone4" runat="server" Text="Done"/>
                </td>
            </tr>
        </table>

    </asp:Panel>

    <asp:Panel ID="Tariff" runat="server" CssClass="tooltip" ToolTip="Select one of the (5) categories on the tariff dropdown list, on selection of a category the tariff scale (sub-category) shall populate with that selections relative values" 
        GroupingText="Tariff <img src='Images/help.png' Width='25px' Height='25px'/>" Width="100%" style="font-size: medium">

        <table class="style1">
            <tr>
                <td class="style2">
                    <asp:Label ID="Label42" runat="server" Text="Tariff :"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTariff" runat="server" Width="50%" 
                        CssClass="style2" onselectedindexchanged="ddlTariff_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Value="0">Please select a Tariff...</asp:ListItem>
                        <asp:ListItem Value="Residential">Residential</asp:ListItem>
                        <asp:ListItem Value="Business and General">Business and General</asp:ListItem>
                        <asp:ListItem Value="Bulk">Bulk</asp:ListItem>
                        <asp:ListItem Value="Other">Other</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label44" runat="server" Text="Tariff Scale :"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTariffScale" runat="server" Width="50%" 
                        CssClass="style2" >
                    <asp:ListItem Value="0">Please select a Scale...</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtTariffScale" runat="server" Width="50%" CssClass="style2" Visible="false"></asp:TextBox>

                </td>
            </tr>
        </table>
        <table class="style1">
            <tr>
                <td class="style2">
                    <asp:Label ID="Label43" runat="server" Text="No of Phases:"></asp:Label>
                </td>
                <td>
                    <%--<asp:RadioButton ID="rdbtnSingle" runat="server" Text="Single" CssClass="style2" />--%>
                    <asp:RadioButtonList ID="rdbtnPhases" ToolTip="The electrical supply can have either single or three phases, residential houses shall use single phase, and business with large loads should use three phases." runat="server" CssClass="style2 tooltip" RepeatDirection="Horizontal">
                        <asp:ListItem Value="Single">Single</asp:ListItem>
                        <asp:ListItem Value="Three">Three</asp:ListItem>
                    </asp:RadioButtonList>
                    
                </td>
                <td>
                    <%--<asp:RadioButton ID="rdbtnThree" runat="server" Text="Three" CssClass="style2"/>--%>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    
                </td>
                <td>
                    <asp:Button ID="btnDone5" runat="server" Text="Done"/>
                </td>
            </tr>
        </table>

    </asp:Panel>

    <asp:Panel ID="TypeOfSupply" runat="server" CssClass="tooltip" ToolTip="Residential customers shall ignore this field. This field is for large power users such as businesses. A kVA is 1,000 Volt-Amperes referred to as apparent power. User shall select a radio button option between metered or unmetered (Note: All electrical connections shall be metered except for unmetered/temporary supplies that are less than 14Days (Installed/Modified) "
        GroupingText="Type Of Supply <img src='Images/help.png' Width='25px' Height='25px'/>" Width="100%" style="font-size: medium">

        <table class="style1">
            <tr>
                <td style="width:100%;">
                    <%--<asp:RadioButton ID="rdbtnMetered" runat="server" Text="Metered" CssClass="style2"/>--%>
                    <asp:RadioButtonList ID="rdbtnTypeOfSupply" runat="server" CssClass="style2" RepeatDirection="Horizontal" Width="100%">
                        <asp:ListItem Value="Metered">Metered</asp:ListItem>
                        <asp:ListItem Value="Unmetered">Unmetered</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
        <table class="style1">
            <tr>
                <td class="style2">
                    <asp:Label ID="Label40" runat="server" CssClass="tooltip" ToolTip=" This is for pure Lighting Applications where the quantity and wattage of lamps used shall be stated. Where low wattage consumption is required, consumer to provide actual current requirement." Text="Requested Demand (kVA) :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtRequestedDemand" runat="server" Width="50%" CssClass="style2 tooltip" ToolTip=" This is for pure Lighting Applications where the quantity and wattage of lamps used shall be stated. Where low wattage consumption is required, consumer to provide actual current requirement."></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label41" runat="server" Text="Requested Energy (kW) :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtRequestedEnergy" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    
                </td>
                <td>
                    <asp:Button ID="btnDone6" runat="server" Text="Done"/>
                </td>
            </tr>
        </table>

    </asp:Panel>

    <asp:Panel ID="DetailsOfMainSwitch" runat="server" CssClass="tooltip" ToolTip="This section is best suited for an electrical contractor to complete and advise a customer. Voltage: Single phase Voltage – 230 V or Three phase Voltage – 400 V. Current: maximum load intended for the supply. Fault Rating is found on the circuit breaker. Protective device: refers to a circuit breaker or isolator that shall be present. If none is found on site, an appropriate protective device shall be installed. " 
        GroupingText="Details of Main Switch <img src='Images/help.png' Width='25px' Height='25px'/>" Width="100%" 
        style="font-size: medium">

        <%--<table class="style1">
            <tr>
                <td class="style2">
                    <asp:Label ID="Label52" runat="server" Text="Details of Main Switch :"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlDetailsOfMainSwitch" runat="server" Width="50%" CssClass="style2">
                        <asp:ListItem Value="0">Please select details...</asp:ListItem>
                        <asp:ListItem Value="Existing entire site">Existing entire site</asp:ListItem>
                        <asp:ListItem Value="Proposed entire site">Proposed entire site</asp:ListItem>
                        <asp:ListItem Value="Existing for this app">Existing for this app</asp:ListItem>
                        <asp:ListItem Value="Proposed for this app">Proposed for this app</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label53" runat="server" Text="Voltage (V) :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtVoltage" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label54" runat="server" Text="Current (A) :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCurrent" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label55" runat="server" Text="Fault Rating (kA) :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFaultRating" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label56" runat="server" Text="Protection Device :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtProtectionDevice" runat="server" Width="50%" CssClass="style2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    
                </td>
                <td>
                    <asp:Button ID="btnDone7" runat="server" Text="Done"/>
                </td>
            </tr>
        </table>--%>

        <table border="1" >
            <tr style="width:100%;">
                <td>
                    <asp:Label ID="Label51" runat="server" Text="Details of main switch"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label52" runat="server" Text="Voltage(V)"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label53" runat="server" Text="Current(A)"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label54" runat="server" Text="Fault Rating(kA)"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label55" runat="server" Text="Protective Device"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label56" runat="server" Text="Existing entire site"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt1by1" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt1by2" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt1by3" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt1by4" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label57" runat="server" Text="Proposed entire site"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt2by1" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt2by2" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt2by3" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt2by4" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label58" runat="server" Text="Existing for this app"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt3by1" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt3by2" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt3by3" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt3by4" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label59" runat="server" Text="Proposed for this app"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt4by1" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt4by2" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt4by3" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt4by4" runat="server"></asp:TextBox>
                </td>
            </tr>

        </table>

    </asp:Panel>

    <table class="style1">
    <tr>
        <asp:FileUpload ID="FileUpload2" runat="server" />
    </tr>
        <tr>
            <td class="style2">
                <asp:Button ID="btnSubmit" runat="server" Text="Save" CssClass="style4" />
            </td>
            <td class="style2">
                <asp:Button ID="btnClear" runat="server" Text="Submit" CssClass="style4" />
            </td>
            <td class="style2">
                <asp:Button ID="Button3" runat="server" Text="Clear" CssClass="style4" />
            </td>
        </tr>
        
    </table>

</asp:Content>
